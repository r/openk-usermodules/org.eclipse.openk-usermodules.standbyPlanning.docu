////
******************************************************************************
* Copyright © 2018 Mettenmeier GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
*
*     http://www.eclipse.org/legal/epl-v10.html
*
******************************************************************************
////
:Date: 2018-06-29
:Revision: 1
:source-highlighter: highlightjs
:highlightjs-theme: solarized_dark

// horizontal line
***
//additional style for arc42 help callouts
ifdef::basebackend-html[]
++++
<style>
.arc42help {font-size:small; width: 14px; height: 16px; overflow: hidden; position: absolute; right: 0px; padding: 2px 0px 3px 2px;}
.arc42help::before {content: "?";}
.arc42help:hover {width:auto; height: auto; z-index: 100; padding: 10px;}
.arc42help:hover::before {content: "";}
@media print {
	.arc42help {display:hidden;}
}
</style>
++++
endif::basebackend-html[]

// toc-title definition MUST follow document title without blank line!
:toc-title: Inhaltsverzeichnis

= image:images/open-konsequenz.jpg[openK_logo] 

== Architecture description for "Standby Planning" module


// configure DE settings for asciidoc
include::src/config.adoc[]

include::src/about-arc42.adoc[]

// numbering from here on
:numbered:

<<<<
// 1. Anforderungen und Ziele
include::src/01_introduction_and_goals.adoc[]

<<<<
// 2. Randbedingungen
include::src/02_architecture_constraints.adoc[]

<<<<
// 3. Kontextabgrenzung
include::src/03_system_scope_and_context.adoc[]

<<<<
// 4. Loeungsstrategie
include::src/04_solution_strategy.adoc[]

<<<<
// 5. Bausteinsicht
include::src/05_building_block_view.adoc[]

<<<<
// 6. Laufzeitsicht
include::src/06_runtime_view.adoc[]

<<<<
// 7. Verteilungssicht
include::src/07_deployment_view.adoc[]

<<<<
// 8. Querschnittliche Konzepte
include::src/08_concepts.adoc[]

<<<<
// 9. Entscheidungen
include::src/09_design_decisions.adoc[]

<<<<
// 10. Qualitaet
include::src/10_quality_scenarios.adoc[]

<<<<
// 11. Risiken
include::src/11_technical_risks.adoc[]

<<<<
// 12. Glossar
include::src/12_glossary.adoc[]


