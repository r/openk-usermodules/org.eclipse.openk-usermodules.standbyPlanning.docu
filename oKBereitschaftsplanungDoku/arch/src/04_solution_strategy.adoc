[[section-solution-strategy]]
== Solution Strategy

The module 'Standby Planning' bases on a three-tier architecture:

. *Frontend* - The GUI is implemented as a web-frontend.
. *Backend* - The business functionalities are implemented in the backend tier. It provides the business functions via RESTful Webservices.
. *Database* - The database stores all module specific data.