////
******************************************************************************
* Copyright © 2018 Mettenmeier GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
*include::arch\src\05_building_block_view.adoc[]
*     http://www.eclipse.org/legal/epl-v10.html
*
******************************************************************************
////


= openKonsequenz "StandbyPlanning"  
:Date: 2018-06-29
:Revision: 1

How to get started

* An architectural overview will be found in the <<arch/architecture, architecture documentation>>. 

* Before getting started with the application there will be some third party products to be extended. See <<documentation/admin#KeyCloak, KeyCloak requirements>>. 

* The application needs a data store. Therefore some configuration information will be found in <<howto/config#Postgres, setting up Postgres>>.

* The basic server side configuration is described in <<howto/config#auth&auth, Auth&Auth ff>>.

* Building up the application StandbyPanning you will find some helpful steps in <<howto/build#FrontEnd, Front End building>> and <<howto/build#BackEnd, Back End building>>.

* Everything is done? Then <<howto/run#login, run the application>>.

* If your environment still work's, then you can run some selenium test suits. That will show if every thing is still correct configured. See  <<howto/selenium#Installing, Selenium Plugin for Chrome>>.






